@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h5>Pizarra</h5>
                </div>
            </div>
        </div>


        <div class="col-md-4 ">
            <div class='row'>
                <div class="col-md-4 ">
                    <div class="card">
                        <div class="card-body">
                            <h6>Pdf visor</h6>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div id="app">
                        <div class="card-header">
                            <h1 class="card-title">{{$group->name}} <span class="badge badge-primary float-right">@{{ usersInRoom.length }}</span></h1>
                            <h5>

                                <strong>Usuarios</strong>:
                                @foreach($users as $user)
                                {{$user->name}} ,
                                @endforeach

                            </h5>
                        </div>
                        <chat-log :messages="messages"></chat-log>
                        <chat-composer v-on:messagesent="addMessage"></chat-composer>
                        <input type="hidden" id="group_id" name="group_id" value="{{$group->id}}">
                    </div>
                </div>
            </div>

        </div>
        @endsection
        
