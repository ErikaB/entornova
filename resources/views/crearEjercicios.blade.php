@extends('layouts.app')
<br>
                        <h2>Cambios en alumnos</h2>
                        <br>
                        <form action="updateSQL.php" method="post" enctype="multipart/form-data" name="personal" id="personal" class="needs-validation" novalidate >              
                            <div class="form-group">
                                <label for="matricula" >Matricula:</label>
                                <input class="form-control" id="matricula" name="matricula" type="text" value=" <?php echo $fila["matricula"] ?> " />                             
                            </div>

                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" value=" <?php echo $fila["nombre"] ?> " />
                            </div>

                            <div class="form-group">
                                <label for="apellidoPaterno">Apellido paterno</label>
                                <input class="form-control"  id="apellidoPaterno" name="apellidoPaterno" type="text" value=" <?php echo $fila["apellidoPaterno"] ?> " />
                           </div>

                            <div class="form-group">
                                <label for="apellidoMaterno">Apellido Materno:</label>
                                <input class="form-control" id="apellidoMaterno" name="apellidoMaterno" type="text" value=" <?php echo $fila["apellidoMaterno"] ?> "  />  
                            </div>
                        <input value="Guardar" type="submit" class="btn btn-primary" /> 
                        </form>        
                  
@section('content')

<div class="container">

  @if(session('status'))
  <div class="alert alert-success" role="alert">
      The Workplace was created successfully!
  </div>
  @endif

    <h1>Create Workplace</h1>
    <form action="http://127.0.0.1:8000/saveWorkplace" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group col-md-8">
        <label for="name">Name:</label>
        <input type="text" name="name" class="form-control">
      </div>
      <div class="form-group col-md-8">
        <h5>Users List</h5>
          @foreach($users as $user)
            <input type="checkbox" name="users[]" value="{{$user->id}}"> {{$user->name}} <br>
          @endforeach
      </div>
      <div class="form-group">
        <input type="submit" name="send" value="Create" class="btn btn-primary">
      </div>
    </form>
</div>

@endsection
                       

